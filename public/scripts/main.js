'use strict';

$(document).ready(function () {

  $('#btn_call').on('click', function(){
    var addMessage = firebase.functions().httpsCallable('addMessage');
    // save to RealTimeDB /messages_cloud_call
    addMessage({text: 'Add<span>Text<span>ToCall<br>able'}).then(function(result) {
      // Read result of the Cloud Function.
      var sanitizedMessage = result.data.text;
    }).catch(function(error) {
      // Getting the Error details.
      var code = error.code;
      var message = error.message;
      var details = error.details;
      console.log(error)
    });
  })

})
